# Prioritize uns tickets

## Overview

1. Compute geohash time spent (on 3 months one-second lat-lng data).
	1. Get the list of journey_id, section, vehicle_number, start time, stop time for all the trips that occured in the given 3 month window.  --> src: vehicle_node_tracking mysql db.
	2. Get the one-second lat, lng data. Contains sourceId (vehicle number), lat, lng, ts. --> src: s3
	3. Join 1 & 2.
	4. Compute geohash 7 from lat, lng.
	5. For each trip (identified by journey_id, section) we have the sequence of geohashes it passed through, and the time-spent in each geohash.

2. For each section, geohash we have a list of trips, and time spent by each of these. Compute the true_max (window_size = 5 min, support = 5 distinct days).

3. Cluster neighboring geohashes having similar characteristics to form nodes.
	1. Remove all those geohashes ( section, geohash pairs ) for which true_max < 3 min.
	2. Need to the cluster the remaining geohashes.
	3. Right now custom hard-coded logic for clustering --> will need to improve this in future.
	4. Use google api to get the list of resturants, tolls, check-posts in the clusters.

4. Compute the time-spent in the clusters.
	1. Compute the statistics (mean, media, percentiles) and true_maxes in the clusters.

5. validation phase -
	1. Collect the uns tickets generated in a one-month period. --> src: ticketing_v2 db, stoppage_alerts.
	2. Merge 1 with trips in that period to also get the journey_id, section also.
	3. For each ticket associate, find the cluster in which the stoppage occured.
	4. Check if we would have genrated the ticket based on the true_max logic.




