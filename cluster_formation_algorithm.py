"""
contains the method used to generate clusters
from geohashes

Input: df containing section, geohash, true_max
Output: df containing geohash, cluster mapping
"""

# Parameters of the algo
MIN_GEOHASH_TRUE_MAX_MINS = 3
NEIGHBOR_MIN_TRUE_MAX_FRAC = 0.25
NEIGHBOR_MIN_TRIPS_FRAC = 0.6

import cgeohash

from pyspark.sql.functions import pandas_udf, PandasUDFType
import pyspark.sql.functions as F

import pandas as pd
from itertools import chain

def compute_frac_of_larger_value(a, b):
    """
    if a is larger, computes what fraction of a is b
    and vice a versa
    """

    diff = abs(a - b)
    larger = max(a, b)
    frac = 1 - diff / larger
    return frac, larger


def is_neighbors_same_cluster(r1, r2):
    """
    given two neighboring geohashes
    defines the criteria if the should be considered
    part of the same geohash
    """
    assert r1.geohash != r2.geohash
    assert r2.geohash in cgeohash.neighbors(r1.geohash), "should be neighbors"

    # true of should be alteast ?% of the other
    if compute_frac_of_larger_value(r1.true_max, r2.true_max)[0] <\
     NEIGHBOR_MIN_TRUE_MAX_FRAC:
        return False
    # number of trips should be alteast ?% of the other
    if compute_frac_of_larger_value(r1.num_trips, r2.num_trips)[0] <\
     NEIGHBOR_MIN_TRIPS_FRAC:
        return False
    return True


def verify_same_cluster_map_sanity(cluster_map):
    """
    function to sanity check the same cluster mapping
    """
    # sanity check on ->
    # 1. no g, g
    # 2. g1, g2 should be neighbors
    # 3. g1, g2 => g2, g1 must also exist
    should_see = set()
    for g1, l in cluster_map.items():
        for g2 in l:
            assert g1 != g2
            assert g2 in cgeohash.neighbors(g1)
            assert g1 in cgeohash.neighbors(g2)
            if (g1, g2) in should_see:
                should_see.remove((g1, g2))
            else:
                should_see.add((g2, g1))
    assert len(should_see) == 0
    return True

def sec_same_cluster_neighbors_map(df):
    """
    given a pandas df for containing the data for a given section,
    compute the same cluster neighbors map.
    it is a mapping from gh -> list
    of all neighboring geohashes of gh, such that they belong to the same cluster
    """

    assert df.section.nunique() == 1, "should contain data for only one section"
    # also for sanity check that each geohash is present in only one row
    assert df.shape[0] == df.geohash.nunique(), "some geohash present in multiple rows"
    
    cluster_map = {} # the same cluster neighbors mapping
    seen = set()
    for i, (_, r1) in enumerate(df.iterrows()):
        #print(i, r1.geohash)
        for ngh in cgeohash.neighbors(r1.geohash):
            if (r1.geohash, ngh) in seen:
                continue
            fdf = df[df.geohash == ngh]
            assert fdf.shape[0] <= 1
            if fdf.shape[0] == 0:
                continue
        
            r2 = fdf.iloc[0]            

            if is_neighbors_same_cluster(r1, r2):
                cluster_map.setdefault(r1.geohash, set()).add(r2.geohash)
                cluster_map.setdefault(r2.geohash, set()).add(r1.geohash)
            seen.add((r1.geohash, r2.geohash))
            seen.add((r2.geohash, r1.geohash))
            
    assert verify_same_cluster_map_sanity(cluster_map)
    return cluster_map

@pandas_udf("gh1 string, gh2 string", PandasUDFType.GROUPED_MAP)
def sec_same_cluster_neighbors_map_udf(df):
    m = sec_same_cluster_neighbors_map(df)
    return pd.DataFrame(data=list(chain.from_iterable(
        ((g1, g2) for g2 in l) for g1, l in m.items()
    )), columns=['gh1', 'gh2'])


def compute_clusters(all_geohashes, cluster_map):
    # from the cluster map get the list of clusters
    clusters = []
    ci_map = {} # contains mapping from geohash to its cluster index 
    # (index in the clusters array)
    
    def add_to_cluster(gh, cluster, cluster_index):
        # helper function to add a node and its neighbors to ci_map
        if gh in ci_map:
            return

        # add this geohash to the cluster
        cluster.add(gh)
        ci_map[ gh ] = cluster_index

        # add its neighbors to the cluster as well
        for ogh in cluster_map.get(gh, []):
            add_to_cluster(ogh, cluster, cluster_index)

    for i, gh in enumerate(all_geohashes):
        # print(i, gh)
        
        if gh in ci_map:
            continue

        tc = set()
        tci = len(clusters)
        add_to_cluster(gh, tc, tci)
        clusters.append(tc)
        
    return clusters, ci_map

def merge_create_cluster(true_maxes_df, same_cluster_df):
    """
    input -> spark df containing the geohash, neighbor geohash, is_same_cluster
    """
    
    same_cluster_map = same_cluster_df.groupby("gh1").agg(
            F.collect_set("gh2").alias("same_cluster_neighbors")
        ).toPandas().set_index('gh1').to_dict()["same_cluster_neighbors"]
    assert verify_same_cluster_map_sanity(same_cluster_map)

    all_geohashes = true_maxes_df.select("geohash").distinct().toPandas().geohash
    _, ci_map = compute_clusters(all_geohashes, same_cluster_map)
    return pd.DataFrame(columns=['geohash', 'cluster'], data=list(ci_map.items()))

def get_clusters(df):
    """
    input df -> contains section, geohash, true_max
    output pandas df -> geohash, cluster
    """

    # first filter out those entries in which true_max is less than the threshold
    df = df.filter(F.col('true_max') >= 60*MIN_GEOHASH_TRUE_MAX_MINS)

    # then compute neighbor same cluster mapping
    # get mapping from each section independently and merge them together
    same_cluster_df = df.groupby('section').apply(sec_same_cluster_neighbors_map_udf)
    return merge_create_cluster(df, same_cluster_df)
