"""
in this will be computing true maxes using distinct days logic
also to be able to compute stats on a geohash, section pair we'll consider atleast 10 trips (instead of the earlier 50 trips)
"""


from datetime import datetime as dt
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SQLContext

import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.functions import pandas_udf, PandasUDFType

import psycopg2
import pymysql
import pandas as pd


# will copy the one second gps data as df

# will run this on cluster with 4 executors each of m4.2xlarge
conf = SparkConf()\
        .setAppName("a1--stoppage_alerts_complete_pipeline")\
        .setMaster("yarn")\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "12")\
        .set("spark.executor.memory", "21000M")\
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .set("spark.io.compression.codec", "snappy")\
        .set("spark.driver.memory", "8G")\
        .set("spark.driver.maxResultSize", "6G")\
        .set("spark.rpc.message.maxSize", "200")\
        .set("spark.yarn.maxAppAttempts", 1)\
        .set("spark.kryoserializer.buffer.max", "200M")


sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

######################################################
################### read inputs #####################
geohash_time_spent_sdf = sqlContext.read.parquet("s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/2/geohash_time_spent_sdf")
#####################################################
print("====> read input will try and perform actual computation")


base_dir = "s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/2_4/"
def save(var, save_name=None):
    if save_name is None:
        save_name = var
    """
    simple function that avoids retyping multiple times.
    saves df as parquet and then loads it again
    """
    print("saving - " + str(var))
    globals()[var].write.parquet(base_dir + save_name)
    globals()[var] = sqlContext.read.parquet(base_dir + save_name)

# min_num_trips = 10
# fil_geohashes = geohash_time_spent_sdf.groupby("section", "geohash").agg(F.countDistinct("ujid").alias("num_trips")).filter(
#     F.col("num_trips") >= min_num_trips
# ).join(
#     geohash_time_spent_sdf,
#     ["section", "geohash"]
# )
# save("fil_geohashes")
fil_geohashes = geohash_time_spent_sdf


def get_true_max_computer_v2(min_support_size=3, support_time_window_size=10):
    @pandas_udf("section string, geohash string, num_trips long, true_max double, support_num_trips long", PandasUDFType.GROUPED_MAP)
    def compute_true_max_v2(df):
        # support_time_window_size = 5 # will check with ? minutes
        # min_support_size = 5 # atleast ? many trips (on distinct days) should have supported this max value for it be now marked outlier  

        # contains data for a given section, geohash
        # verify that we have data for only section, geohash
        assert df.groupby(["section", "geohash"]).size().shape[0] == 1

        df['day'] = (df.st /1000 + 5.5*60*60).map(lambda ts: dt.fromtimestamp(ts).strftime("%Y-%m-%d"))
        
        # TODO: will consider the times for all the 8 neighbors in v3
        df = df.sort_values("time_spent_sec", ascending=False)
        true_max = None
        support_size = None
        for _, row in df.iterrows():
            ts = row.time_spent_sec
            min_ts = ts - 60*support_time_window_size
            max_ts = ts + 60*support_time_window_size

            support_df = df.query("@min_ts <= time_spent_sec <= @max_ts")
            support_trips = support_df.day.nunique()
            if support_trips >= min_support_size:
                # got our true max
                true_max = ts
                support_size = support_trips
                break
        return pd.DataFrame(columns=['section', 'geohash', 'num_trips', 'true_max', 'support_num_trips'], data=[
            (df.section.iloc[0], df.geohash.iloc[0], df.ujid.nunique(), true_max, support_size)
        ])
    
    return compute_true_max_v2


import cgeohash

@F.udf("array<string>")
def neighbors(gh):
    # computes the 8 neighbors of gh
    nl = cgeohash.neighbors(gh)
    nl.append(gh) # also include self in that list
    return nl

def compute_true_max_v3(df):
    # this takes in a time spent df
    # and computes the true maxes and that takes into account the neighboring
    # geohases as well

    return df.withColumn("geohash", neighbors("geohash")).withColumn("geohash", F.explode("geohash"))\
        .groupby("section", "geohash").apply(get_true_max_computer_v2())


print("===> computing true maxes")
# true_max_times_v2 = fil_geohashes.groupby("section", "geohash").apply(get_true_max_computer_v2())
true_max_times_v3 = compute_true_max_v3(fil_geohashes)
save("true_max_times_v3")
print("true maxes computed!")

print("====> done!")







