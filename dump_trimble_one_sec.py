from datetime import datetime as dt
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SQLContext

import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.functions import pandas_udf, PandasUDFType

import psycopg2
import pymysql
import pandas as pd


# will copy the one second gps data as df

# will run this on cluster with 4 executors each of m4.2xlarge
conf = SparkConf()\
        .setAppName("trimble_one_sec_to_df_1")\
        .setMaster("yarn")\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "12")\
        .set("spark.executor.memory", "21000M")\
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .set("spark.io.compression.codec", "snappy")\
        .set("spark.driver.memory", "8G")\
        .set("spark.driver.maxResultSize", "6G")\
        .set("spark.rpc.message.maxSize", "200")\
        .set("spark.yarn.maxAppAttempts", 1)\
        .set("spark.kryoserializer.buffer.max", "200M")


sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

######################################################
month_path = lambda month: "s3://rivigo-sensor-data/trimble-fuel-continuous/year=2018/month={}/day=*/*/*.gz".format(month)
# collect data of sept, oct, nov, dec
paths = [month_path(month) for month in ["09", "10", "11", "12"]]
ones_sample = sc.textFile(",".join(paths))

def parse_one_sec_line(l):
    ls = l.split("|")
    return Row(
        sourceId=ls[0],
        ts_ms=int(ls[1]),
        fuel=float(ls[2]),
        speed=float(ls[3]),
        lat=float(ls[4]),
        lng=float(ls[5])
    )
ones_df= ones_sample.map(parse_one_sec_line).toDF()

out_path = "s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/one_sec_data_2018_sept_to_dec"
ones_df.write.parquet(out_path)

