#!/usr/bin/env python3

"""
script to dump place types from the google api
calls the google places api to get the things around a given lat lng

sample api -->
https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=20.124893188476562,78.60786437988281&radius=150&key=AIzaSyCpYSf6Km6cj6COt6vfIvNjfSBtpl_zhsY&type=restaurant
(if no type is specified then google returns all possible things nearby)

since couldn't find a type for toll etc. can do a keyword search on all the places
to find the tolls and the checkpost / borders.
"""

import pandas as pd
import requests
import time
import geohash2
import pickle
import sys
import signal

# load the data frame which is the source of the geohashes whose types are to be dumped
d2 = pd.read_pickle('../results/all_clusters.pk')
# commment below if want to run for all
# d2 = d2.iloc[:10]

print("# rows in df - ", d2.shape[0])
print("# of distinct geohashes - ", d2.geohash.nunique())

try:
    with open('rdict.pk', 'rb') as f:
        rdict = pickle.load(f)
        print("using already present response file")
except:
    print("absent / unreadable response file")
    rdict = {}
    rdict['all'] = {}
    rdict['restaurant'] = {}

# these two dicts shall contain the individual results
rall = rdict['all']
rres = rdict['restaurant'] 

def save():
    with open('rdict.pk', 'wb') as f:
        pickle.dump(rdict, f)

def signal_handler(sig, frame):
    print("\ncaught keyboard interrupt. saving file...")
    save()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

def is_res_suc(r):
    if r is None:
        return False, "response is null"
    if r.status_code != 200:
        return False, "status code - {}".format(r.status_code)
    try:
        status = r.json()['status']
        if status in ['OK', 'ZERO_RESULTS']:
            return True, "status - {}    ".format(status)
        else:
            return False, "status - {}".format(status)
    except:
        return False, "invalid / unexpected res json"

n_suc = 0
n_fail = 0
def print_res(i, gh, r):
    global n_suc, n_fail    
    success, comment = is_res_suc(r)
    n_suc += int(success)
    n_fail += int(not success)
    print("\r[{}/{} ({:.2f} %)] ({} reqs suc, {} reqs fail) {} - {}".format(
        i+1, d2.shape[0], 100*(i+1)/d2.shape[0], n_suc, n_fail, gh, comment
    ), end="")


for i, gh in enumerate(d2.geohash):
    lat, lng, *_others = geohash2.decode_exactly(gh)
    
    if not is_res_suc(rres.get(gh, None))[0]:
        r = requests.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&radius=150&key=AIzaSyCpYSf6Km6cj6COt6vfIvNjfSBtpl_zhsY&type=restaurant".format(
            lat=lat, lng=lng
        ))
        rres[gh] = r
        print_res(i, gh, r)
        time.sleep(0.1)

    if not is_res_suc(rall.get(gh, None))[0]:
        r = requests.get("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={lat},{lng}&radius=150&key=AIzaSyCpYSf6Km6cj6COt6vfIvNjfSBtpl_zhsY".format(
            lat=lat, lng=lng
        ))
        rall[gh] = r
        print_res(i, gh, r)
        time.sleep(0.1)

print()
save()
