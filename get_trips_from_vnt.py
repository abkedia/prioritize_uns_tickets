"""
Given some time window, find all trips (journey_id, section, vehicle_number, start_ts, end_ts) 
that start and end within that window
"""

################ code to get data from mysql ########################

import pymysql
import pandas as pd

## Ordered dict cursor for pymysql
# refer -> https://stackoverflow.com/questions/33504938/pymysql-and-ordereddict
from collections import OrderedDict
from pymysql.cursors import DictCursorMixin, Cursor
class OrderedDictCursor(DictCursorMixin, Cursor):
    dict_type = OrderedDict

connect_data_ana = lambda: pymysql.connect(
    host='prime-data-analytics.c90oafloutlv.ap-southeast-1.rds.amazonaws.com',
    user='pilot_continuum_api',
    password='api@pilotcontinuum',
    db='pilot_continuum',
    cursorclass=OrderedDictCursor
)

connect_rivigo_prod = lambda: pymysql.connect(
    host='rivigo-prod-replica.c90oafloutlv.ap-southeast-1.rds.amazonaws.com',
    user='unloading',
    password='unloading',
    db='rivigo',
    cursorclass=OrderedDictCursor
)


def get_mysql_df(query, args=tuple(), connect=connect_rivigo_prod):
    with connect() as cur:
        cur.execute(query, args)
        r = cur.fetchall()
    if len(r) != 0:
        return pd.DataFrame(r)
    else:
        return pd.DataFrame(columns=[ e[0] for e in cur.description ])
################################################################

def get_trips_from_vnt(start_ts, end_ts):
    """
    start_ts and end_ts are the unix timestamps (GMT in seconds) that define the window
    """
    
    # goal is to find all the journey_id, sections that stant and end within the start_ts, end_ts window

    # right now node types are PITSTOP, CIENT_WAREHOUSE and FUEL_PUMP
    # we are going to remove the FUEL_PUMP nodes for computing sections
    query = """
        select 
            vnt.id as vnt_id, journey_id, vnt.vehicle_number, vnt.node_type, vnt.node_id, 
            h.hub_code, vnt.stop_sequence, vnt.actual_in_timestamp, vnt.actual_out_timestamp
        from rivigo.vehicle_node_tracking vnt
        left join rivigo.Hub h on (vnt.node_type = 'PITSTOP' and h.id = vnt.node_id)
        where
            node_type in ('PITSTOP', 'CLIENT_WAREHOUSE')
            and journey_type = 'TRIP'
            and actual_out_timestamp >= {ts}
            and actual_in_timestamp <= {te}    
            and vnt.is_active = 1 
        order by journey_id asc, stop_sequence asc;
    """.format( 
        ts = start_ts*1000, # 1000 required since actual_out_time is in ms
        te = end_ts*1000 
    )

    fil_vnt_nodes_orig = get_mysql_df(query)

    print()
    print("number of filtered nodes = ", fil_vnt_nodes_orig.shape[0])
    bad_nodes_filter = fil_vnt_nodes_orig.actual_in_timestamp > fil_vnt_nodes_orig.actual_out_timestamp # out should come after in
    num_bad_nodes = fil_vnt_nodes_orig[ bad_nodes_filter ].shape[0]
    print( "number of invalid nodes = {} ({:.2f}%)".format( num_bad_nodes, 100*num_bad_nodes / fil_vnt_nodes_orig.shape[0]  ))

    fil_vnt_nodes = fil_vnt_nodes_orig[ ~bad_nodes_filter ]
    print("number of valid vnt nodes = ", fil_vnt_nodes.shape[0])

    def get_journey_id_sections_from_vnt_nodes(fil_vnt_nodes):
        d2 = fil_vnt_nodes.copy(deep=True).shift(-1)
        d3 = pd.merge(fil_vnt_nodes, d2, left_index=True, right_index=True)
        d4 = d3[ d3.journey_id_x == d3.journey_id_y ]
        print("intial number of sections =", d4.shape[0])

        d5 = d4[ d4.actual_out_timestamp_x <= d4.actual_in_timestamp_y ]
        print("physically possible sections = {} [ {:.2f}% ]".format( d5.shape[0], 100*d5.shape[0] / d4.shape[0] ))

        d6 = d5[ d5.node_id_x != d5.node_id_y ]
        print("after removing self sections =", d6.shape[0])

        d7 = d6[ (d6.node_type_x == 'PITSTOP') & (d6.node_type_y == 'PITSTOP') ]
        print("ps-to-ps sections =", d7.shape[0])

        fil_sections = d7[ 
            (d7.actual_out_timestamp_x >= start_ts*1000) &
            (d7.actual_in_timestamp_y <= end_ts*1000) 
        ]
        print("sections starting and ending inside the window =", fil_sections.shape[0])

        def check_and_transform_sections_df(df):
            df2 = df[ 
                (df.journey_id_x == df.journey_id_y)
                & (df.vehicle_number_x == df.vehicle_number_y)
                & (df.node_type_x == 'PITSTOP')
                & (df.node_type_y == 'PITSTOP')
                & (~df.hub_code_x.isnull())
                & (~df.hub_code_y.isnull())
                & (df.actual_in_timestamp_x <= df.actual_out_timestamp_x )
                & (df.actual_in_timestamp_y <= df.actual_out_timestamp_y )
                & (df.actual_out_timestamp_x <= df.actual_in_timestamp_y)
            ]
            assert df.shape[0] == df2.shape[0]

            df3 = pd.DataFrame()
            df3['journey_id'] = df['journey_id_x'].astype(int)
            df3['vehicle_number'] = df['vehicle_number_x']
            df3['src_ps'] = df['hub_code_x']
            df3['dest_ps'] = df['hub_code_y']
            df3['section'] = df['hub_code_x'] + '-' + df['hub_code_y']
            # df3["hub_id_pair"] = df.node_id_x.astype("str") + "_PITSTOP_" + (df.node_id_y.astype("int")).astype("str") + "_PITSTOP"
            df3['start_ts_unixms'] = df['actual_out_timestamp_x'].astype(int)
            df3['end_ts_unixms'] = df['actual_in_timestamp_y'].astype(int)
            return df3

        jid_sections_df = check_and_transform_sections_df(fil_sections)
        jid_sections_df.reset_index(drop=True, inplace=True)
        jid_sections_df['ujid'] = jid_sections_df.index
        return jid_sections_df
        
    jid_sections = get_journey_id_sections_from_vnt_nodes(fil_vnt_nodes)
    print("number of trips ==> " + str(jid_sections.shape[0]))

    return jid_sections


from datetime import datetime as dt

def get_zoom_trips_vnt(start_ts, end_ts, silent=False):
    """
    start_ts and end_ts are unix timestamps (GMT seconds)
    """

    # just to check if start and end are valid timestamps
    # will raise exception if either one is invalid
    dt.fromtimestamp(start_ts)
    dt.fromtimestamp(end_ts)

    # actual query
    vnt_nodes = get_mysql_df("""
        select
          vnt.id,
          vnt.journey_id,
          lht.client_code,
          vnt.node_type,
          if(vnt.node_type = 'PITSTOP', concat('P_', h1.hub_code), 
            concat('C_', cwh.name)) as node_code,
          from_unixtime(vnt.actual_in_timestamp/1000) as tin,
          from_unixtime(vnt.actual_out_timestamp/1000) as tout
        from vehicle_node_tracking vnt
        join line_haul_trip lht on (vnt.journey_id = lht.id)
        left join Hub h1 on (vnt.node_type = 'PITSTOP' and vnt.node_id = h1.id)
        left join Client_Ware_House cwh on (vnt.node_type = 'CLIENT_WAREHOUSE'
            and vnt.node_id = cwh.id)
        where node_type in ('PITSTOP', 'CLIENT_WAREHOUSE')
          and actual_out_timestamp >= {t1}*1000
          and actual_in_timestamp <= {t2}*1000
          and lht.client_code = 'RZM'
          and vnt.is_active
          and lht.is_active
        order by journey_id, stop_sequence, scheduled_in_timestamp;
    """.format(t1=start_ts, t2=end_ts))

    # some sanity checks
    # ids should be unique
    distinct_ids = vnt_nodes['id'].nunique()
    if distinct_ids != vnt_nodes.shape[0]:
        # some ids are probably repeated
        s = vnt_nodes.groupby('id').size()
        s = s[ s > 1 ]
        assert False, "got duplicate ids in result. total rows = {n},"+\
         " distinct ids = {nd}. following vnt ids are repeated -- \n{s}\n"+\
         "try and check why are they getting duplicated, maybe an issue with"+\
         "is_active".format(n=vnt_nodes.shape[0], nd=distinct_ids, s=str(s))

    if not silent:
        print("distinct journey ids found -- " +\
            str(vnt_nodes.journey_id.nunique() ))

    # now in some cases -- tin > tout
    # this might be due to multiple reasons
    # for now handling such cases by setting tout to be equal to tin
    def handle_tin_gt_tout(df):
        mask = df.tin > df.tout
        df.loc[mask, 'tout'] = df[mask]['tin']
    handle_tin_gt_tout(vnt_nodes)


    v1 = vnt_nodes.shift(-1)[:-1]
    m = vnt_nodes.merge(v1, left_index=True, right_index=True)
    m = m.query("journey_id_x == journey_id_y").copy()

    if not silent:
        print("# of journey ids with > 1 nodes -- " +\
            str(m.journey_id_x.nunique()))
        print("total journey id, section pairs -- " + str(m.shape[0]))

    # right now for the section assuming time in when in at the src node
    # time out is when out the dest node
    m['sec_tin'] = m.tin_x
    m['sec_tout'] = m.tout_y

    # handle cases where tin > tout
    # for now ignoring all of such jid, section pairs
    m = m.query('sec_tin < sec_tout')
    if not silent:
        print("# of valid jid, section pairs -- " + str(m.shape[0]))

    # TODO:
    # think about if there are other invalid entries in this?
    # also should we check if there is overlap in the times for same journey_id /
    # vehicle number
    # -- ideally we should do this, and try and remove overlap as much as possible

    ans = pd.DataFrame()
    ans['id'] = m.id_x.astype(str) + ':' + m.id_y.astype(int).astype(str)
    ans['journey_id'] = m.journey_id_x
    ans['src_node'] = m.node_code_x
    ans['dest_node'] = m.node_code_y
    ans['section'] = ans.src_node + '<-->' + ans.dest_node
    ans['tin'] = m.sec_tin
    ans['tout'] = m.sec_tout

    return ans


