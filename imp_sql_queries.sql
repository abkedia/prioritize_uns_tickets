# find out the section for a given journey and time 
# / given vehicle_number and ts, figure out journey_id, section
# uses vc_replica as source of truth

select 
  trip_id, 
  client,
  section,
  source_id,
  from_unixtime(min(event_start_at)/1000 + 5.5*60*60) as start_ist,
  from_unixtime(max(event_end_at)/1000 + 5.5*60*60) as end_ist
from vc_replica
where trip_id = 8015494
group by section, source_id
order by start_ist;



