"""
some common stuff to minimize typing when running in the notebook
"""

# out of scope here --
#	1. memory / spark-instance leak due to running spark inside a notebook


S3_BASE = 's3a://'
DISK_BASE = '/tmp/spark_nb/'


# first setup so that we can find and import pyspark
try:
	import pyspark
except ImportError:
	import sys
	sys.path.append("/usr/lib/spark/python")
	import pyspark

from pyspark import SparkContext
from pyspark.sql import SQLContext

sc = None # will hold a reference to the singleton spark context
sw = None # will hold a reference to the single main class SW
class SW:
	"""

	"""

	def __new__(cls, *args, **kwargs):
		# to ensure that we have a singleton
		# inspired from --> https://howto.lintel.in/python-__new__-magic-method-explained/
		# lots of other ways to create singleton, e.g. see -> 
		# https://stackoverflow.com/questions/6760685/creating-a-singleton-in-python
		# or google "python singleton" for more techiques

		global sw
		if sw is not None:
			print("")

	def __init__(self):
		global sc
		if sc is None or sc._jsc.sc().isStopped():
			if sc is None and globals()['sc']

			# start a new spark context here



		if sc is not None:
			self.o = _sw
			# also check if o's spark context and sc are stopped or not
			# if yes then restart and update them as well

		else:
			# init and then assign




# create functions to 

