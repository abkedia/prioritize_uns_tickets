# this contains the true max of all sections on 3 months data using params -
# min_support_size=5, support_time_window_size=5
# and this DOES NOT include the neighbors in the computation

tm = sqlContext.read.parquet("s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/2_2/true_max_times_v2")

ftm = tm.filter(F.col('section').isin(["NAGP1-ADBP1", "ADBP1-NAGP1"])).filter(F.col('true_max').isNotNull()).cache()


# caputure the true maxes in the required section
d1 = ftm.filter(F.col('section') == 'NAGP1-ADBP1').toPandas()
d2 = d1[ d1.true_max >= 3*60 ]

######## functions #######

def compute_frac_of_larger_value(a, b):
    diff = abs(a - b)
    larger = max(a, b)
    frac = 1 - diff / larger
    return frac, larger

def is_part_of_cluster(r1, r2):
    
    if r1.geohash == sgeohash or r2.geohash == sgeohash:
        display(pd.DataFrame([ r1, r2 ]))
        print("==>", r1.geohash, r2.geohash, compute_frac_of_larger_value(r1.true_max, r2.true_max)[0], compute_frac_of_larger_value(r1.num_trips, r2.num_trips)[0])
    
    # r1 and r2 must be distinct clusters
    # first g1 and g2 should be "physical" neighbor of each other
    if r2.geohash not in cgeohash.neighbors(r1.geohash):
        return False
    # true of should be alteast 25% of the other
    if compute_frac_of_larger_value(r1.true_max, r2.true_max)[0] < 0.25:
        return False
    # number of trips should be alteast 60% of the other
    if compute_frac_of_larger_value(r1.num_trips, r2.num_trips)[0] < 0.6:
        return False
    return True

def compute_cluster_map(df):
    assert df.shape[0] == df.geohash.nunique()
    cluster_map = {}
    seen = set()
    for i, (_, r1) in enumerate(df.iterrows()):
        #print(i, r1.geohash)
        for ngh in cgeohash.neighbors(r1.geohash):
            if (r1.geohash, ngh) in seen:
                continue
            fdf = df[df.geohash == ngh]
            assert fdf.shape[0] <= 1
            if fdf.shape[0] == 0:
                continue
        
            r2 = fdf.iloc[0]            
            if r1.geohash == r2.geohash or (r1.geohash, r2.geohash) in seen:
                continue
            if is_part_of_cluster(r1, r2):
                cluster_map.setdefault(r1.geohash, set()).add(r2.geohash)
                cluster_map.setdefault(r2.geohash, set()).add(r1.geohash)
            seen.add((r1.geohash, r2.geohash))
            seen.add((r2.geohash, r1.geohash))
            
    return cluster_map

#################

cluster_map = compute_cluster_map(d2)
# ^ contains mapping from gh -> list l of gh
# for a given gh, l -> contains the list of neighboring geohashes 
# which should belong in as gh


def compute_clusters(df, cluster_map):
    # from the cluster map get the list of clusters
    clusters = []
    ci_map = {} # contains mapping from geohash to its cluster index (index in the clusters array)
    
    def add_to_cluster(gh, cluster, cluster_index):
        # helper function to add a node and its neighbors to ci_map
        if gh in ci_map:
            return

        # add this geohash to the cluster
        cluster.add(gh)
        ci_map[ gh ] = cluster_index

        # add its neighbors to the cluster as well
        for ogh in cluster_map.get(gh, []):
            add_to_cluster(ogh, cluster, cluster_index)

    for i, gh in enumerate(df.geohash):
        print(i, gh)
        
        if gh in ci_map:
            continue

        tc = set()
        tci = len(clusters)
        add_to_cluster(gh, tc, tci)
        clusters.append(tc)
        
    return clusters, ci_map



ans = compute_clusters(d2, cluster_map)

d2 = d2.copy()
d2['cluster'] = d2.geohash.map(lambda g: ans[1][g])

# ^ contains the geohash cluster mapping
