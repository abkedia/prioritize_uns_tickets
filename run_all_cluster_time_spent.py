"""
For all the clusters compute the time spent in each cluster
"""

from datetime import datetime as dt
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SQLContext

import pyspark.sql.functions as F
import pyspark.sql.types as T
from pyspark.sql import Row
from pyspark.sql.functions import pandas_udf, PandasUDFType

import psycopg2
import pymysql
import pandas as pd
import numpy as np


# will copy the one second gps data as df

# will run this on cluster with 4 executors each of m4.2xlarge
conf = SparkConf()\
        .setAppName("a1--all_cluster_time_spents")\
        .setMaster("yarn")\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "12")\
        .set("spark.executor.memory", "21000M")\
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .set("spark.io.compression.codec", "snappy")\
        .set("spark.driver.memory", "8G")\
        .set("spark.driver.maxResultSize", "6G")\
        .set("spark.rpc.message.maxSize", "200")\
        .set("spark.yarn.maxAppAttempts", 1)\
        .set("spark.kryoserializer.buffer.max", "200M")


sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)



######################################################
print("====> will try and perform actual computation")

base_dir = "s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/all_clusters_trained_on_3months_data/"
def save(var, save_name=None):
    if save_name is None:
        save_name = var
    """
    simple function that avoids retyping multiple times.
    saves df as parquet and then loads it again
    """
    print("saving - " + str(var))
    globals()[var].write.parquet(base_dir + save_name)
    globals()[var] = sqlContext.read.parquet(base_dir + save_name)

# Part 1 -> compute the time spent in each geohash
# inputs --> 
# 1. geohash cluster mapping
# 2. all lat lngs with geohashes
# output -->
# - time spent by each jid in each of its clusters

geohash_cluster_mapping = sqlContext.read.parquet(base_dir + "geohash_cluster_mapping")
all_latlngs_sdf = sqlContext.read.parquet("s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/2/latlng_with_geohash_base")


latlngs_sdf_f2 = all_latlngs_sdf.join(geohash_cluster_mapping, "geohash", "left_outer")\
                        .withColumn('ncluster', F.when(F.col('cluster').isNotNull(), F.col('cluster').cast(T.StringType()))\
                                                    .otherwise(F.col('geohash')) )


@pandas_udf(
    "ujid long, journey_id long, section string, cluster double, ncluster string, st long, et long, time_spent_sec double",
    PandasUDFType.GROUPED_MAP
)
def compute_time_spent_in_cluster(df):
    df = df.sort_values('ts_ms')
    df['cons_id'] = (df.ncluster != df.ncluster.shift()).cumsum()
    df['n'] = df.groupby('cons_id')['ncluster'].transform(np.size)
    os = df.shape[0]
    df = df[ df.n >= 2 ]
    ns = df.shape[0]
    
    columns = ['ujid', 'journey_id', 'section', 'cluster', 'ncluster', 'st', 'et', 'time_spent_sec']
    if ns == 0:
        return pd.DataFrame(columns=columns)
    
    # after removing the one offs, some may combine again
    df['cons_id'] = (df.ncluster != df.ncluster.shift()).cumsum()
    
    
    def helper_gh_time(df):
        st = df.ts_ms.iloc[0]
        et = df.ts_ms.iloc[-1]
        time_spent_sec = (et - st)/1000
        return pd.DataFrame(columns=columns, data=[
            (df.ujid.iloc[0], df.journey_id.iloc[0], df.section.iloc[0], df.cluster.iloc[0], df.ncluster.iloc[0], st, et, time_spent_sec)
        ])
        
    return df.groupby('cons_id').apply(helper_gh_time).reset_index(drop=True)


cluster_time_spents = latlngs_sdf_f2.groupby('ujid').apply(compute_time_spent_in_cluster)
save("cluster_time_spents", "raw_cluster_time_spent")


processed_cluster_time_spent = cluster_time_spents.filter(F.col('cluster').isNotNull()).groupby('ujid', 'section', 'ncluster').agg(
    F.min('st').alias('st'),
    F.max('et').alias('et'),
    F.sum('time_spent_sec').alias('time_spent_sec')
)
save("processed_cluster_time_spent")


######## Part 1 over ##############

# Part 2
# compute various true maxes for the clusters


def get_cluster_true_max_computer_v2(min_support_size=5, support_time_window_size=5):
    @pandas_udf("section string, ncluster string, num_trips long, true_max double, support_num_days long", PandasUDFType.GROUPED_MAP)
    def compute_true_max_v2(df):
        # support_time_window_size = 5 # will check with ? minutes
        # min_support_size = 5 # atleast ? many trips (on distinct days) should have supported this max value for it be now marked outlier  

        # contains data for a given section, geohash
        # verify that we have data for only section, geohash
        assert df.groupby(["section", "ncluster"]).size().shape[0] == 1

        df['day'] = (df.st /1000 + 5.5*60*60).map(lambda ts: dt.fromtimestamp(ts).strftime("%Y-%m-%d"))
        
        # TODO: will consider the times for all the 8 neighbors in v3
        df = df.sort_values("time_spent_sec", ascending=False)
        true_max = None
        support_size = None
        for _, row in df.iterrows():
            ts = row.time_spent_sec
            min_ts = ts - 60*support_time_window_size
            max_ts = ts + 60*support_time_window_size

            support_df = df.query("@min_ts <= time_spent_sec <= @max_ts")
            support_trips = support_df.day.nunique()
            if support_trips >= min_support_size:
                # got our true max
                true_max = ts
                support_size = support_trips
                break
        return pd.DataFrame(columns=['section', 'ncluster', 'num_trips', 'true_max', 'support_num_days'], data=[
            (df.section.iloc[0], df.ncluster.iloc[0], df.ujid.nunique(), true_max, support_size)
        ])
    
    return compute_true_max_v2


ctm_5 = processed_cluster_time_spent.groupby('section', 'ncluster').apply(get_cluster_true_max_computer_v2(min_support_size=5))

ctm_7 = processed_cluster_time_spent.groupby('section', 'ncluster').apply(get_cluster_true_max_computer_v2(min_support_size=7))

ctm_10 = processed_cluster_time_spent.groupby('section', 'ncluster').apply(get_cluster_true_max_computer_v2(min_support_size=10))


# save these each
save("ctm_5", "cluster_true_max_support_5_days")
save("ctm_7", "cluster_true_max_support_7_days")
save("ctm_10", "cluster_true_max_support_10_days")


# Part 3
# compute the other statistics such as mean, median, other percentile
# and merge them with true maxes

# compute cluster mean, median, 25%, 75%, 90%, 99%, true_max_s5, true_mx_s7
# first mean, median, etc.

cluster_stat1 = processed_cluster_time_spent.withColumn('tm', F.col('time_spent_sec')/60).groupby('section', 'ncluster').agg(
    F.countDistinct('ujid').alias('ntrips'),
    F.mean('tm').alias('mean'),
    F.expr("percentile_approx(tm, 0.5)").alias('median'),
    F.expr("percentile_approx(tm, 0.25)").alias('p25'),
    F.expr("percentile_approx(tm, 0.75)").alias('p75'),
    F.expr("percentile_approx(tm, 0.90)").alias('p90'),
    F.expr("percentile_approx(tm, 0.99)").alias('p99'),
    F.expr("percentile_approx(tm, 0.999)").alias('p99_9'),
    F.max('tm').alias('max')
)

cluster_stat2 = cluster_stat1.join(
    ctm_5.select('ncluster', (F.col('true_max')/60).alias('true_max_s5')), ['section', 'ncluster']).join(
    ctm_7.select('ncluster', (F.col('true_max')/60).alias('true_max_s7')), ['section', 'ncluster']).join(
    ctm_10.select('ncluster', (F.col('true_max')/60).alias('true_max_s10')), ['section', 'ncluster'])
    
save("cluster_stat2", "combined_cluster_stats")
