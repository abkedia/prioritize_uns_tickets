"""
this is will dump and process tickets data
"""


from datetime import datetime as dt
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SQLContext

import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.functions import pandas_udf, PandasUDFType

import psycopg2
import pymysql
import pandas as pd


# will copy the one second gps data as df

# will run this on cluster with 4 executors each of m4.2xlarge
conf = SparkConf()\
        .setAppName("a1--dump_stoppage_tickets")\
        .setMaster("yarn")\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "12")\
        .set("spark.executor.memory", "21000M")\
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .set("spark.io.compression.codec", "snappy")\
        .set("spark.driver.memory", "8G")\
        .set("spark.driver.maxResultSize", "6G")\
        .set("spark.rpc.message.maxSize", "200")\
        .set("spark.yarn.maxAppAttempts", 1)\
        .set("spark.kryoserializer.buffer.max", "200M")


sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)


from dateutil import parser
from datetime import timedelta
from ticketing_db_utils import get_uns_tickets

end = parser.parse("2019 march 14")
start = end - timedelta(days=40)

def saver(df, name):
    base_dir = "s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/tickets_data/fourty_days_upto_march_14_2019/"
    df.write.parquet(base_dir + name)
    return sqlContext.read.parquet(base_dir + name)

print('====> staring actual work...')
get_uns_tickets(sqlContext, start.timestamp(), end.timestamp(), saver=saver)
print('====> done')


