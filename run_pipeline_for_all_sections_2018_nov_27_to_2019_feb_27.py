from datetime import datetime as dt
from pyspark import SparkConf
from pyspark import SparkContext
from pyspark.sql import SQLContext

import pyspark.sql.functions as F
from pyspark.sql import Row
from pyspark.sql.functions import pandas_udf, PandasUDFType

import psycopg2
import pymysql
import pandas as pd


# will copy the one second gps data as df

# will run this on cluster with 4 executors each of m4.2xlarge
conf = SparkConf()\
        .setAppName("a1--stoppage_alerts_complete_pipeline")\
        .setMaster("yarn")\
        .set("spark.executor.instances", "4")\
        .set("spark.executor.cores", "12")\
        .set("spark.executor.memory", "21000M")\
        .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")\
        .set("spark.io.compression.codec", "snappy")\
        .set("spark.driver.memory", "8G")\
        .set("spark.driver.maxResultSize", "6G")\
        .set("spark.rpc.message.maxSize", "200")\
        .set("spark.yarn.maxAppAttempts", 1)\
        .set("spark.kryoserializer.buffer.max", "200M")


sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

######################################################
print("====> will try and perform actual computation")

base_dir = "s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/2/"
def save(var, save_name=None):
    if save_name is None:
        save_name = var
    """
    simple function that avoids retyping multiple times.
    saves df as parquet and then loads it again
    """
    print("saving - " + str(var))
    globals()[var].write.parquet(base_dir + save_name)
    globals()[var] = sqlContext.read.parquet(base_dir + save_name)

# since will be run on 28th feb will have data from jan to feb 2019
one_sec_path = "s3://rivigo-sensor-data/trimble-fuel-continuous/year=2019/month=*/day=*/*/*.gz"
ones_sample = sc.textFile(one_sec_path)

def parse_one_sec_line(l):
    ls = l.split("|")
    return Row(
        sourceId=ls[0],
        ts_ms=int(ls[1]),
        fuel=float(ls[2]),
        speed=float(ls[3]),
        lat=float(ls[4]),
        lng=float(ls[5])
    )
ones_df= ones_sample.map(parse_one_sec_line).toDF()
save("ones_df", "one_sec_data_2019_jan_to_27_feb")

# now our window is last 3 month so we'll need to use datafrom 2018 also
ones_2018 = sqlContext.read.parquet("s3://chronos-sensor-data/data/spark_data/pg-1/unsp/one_sec_data_2018_sept_to_dec/")
ones_df = ones_df.union(ones_2018)


############## code to get trips #############################################
print("=====> starting to fetch the trips in the window ...")
from dateutil import parser

# a 3 month window
# 2018 Nov 27 00:00:00 GMT (05:30 morning in IST)
start_time = parser.parse("2018 Nov 27")

# 2019 Feb 27 00:00:00 GMT (05:30 morning in IST)
end_time = parser.parse("2019 Feb 27")

print(start_time, '->', start_time.timestamp())
print(end_time, '->', end_time.timestamp())



## code to get data from mysql

import pymysql
import pandas as pd

## Ordered dict cursor for pymysql
# refer -> https://stackoverflow.com/questions/33504938/pymysql-and-ordereddict
from collections import OrderedDict
from pymysql.cursors import DictCursorMixin, Cursor
class OrderedDictCursor(DictCursorMixin, Cursor):
    dict_type = OrderedDict
#########

connect_data_ana = lambda: pymysql.connect(
    host='prime-data-analytics.c90oafloutlv.ap-southeast-1.rds.amazonaws.com',
    user='pilot_continuum_api',
    password='api@pilotcontinuum',
    db='pilot_continuum',
    cursorclass=OrderedDictCursor
)

connect_rivigo_prod = lambda: pymysql.connect(
    host='rivigo-prod-replica.c90oafloutlv.ap-southeast-1.rds.amazonaws.com',
    user='unloading',
    password='unloading',
    db='rivigo',
    cursorclass=OrderedDictCursor
)


def get_mysql_df(query, args=tuple(), connect=connect_rivigo_prod):
    with connect() as cur:
        cur.execute(query, args)
        r = cur.fetchall()
    if len(r) != 0:
        return pd.DataFrame(r)
    else:
        return pd.DataFrame(columns=[ e[0] for e in cur.description ])


# goal is to find all the journey_id, sections that stant and end within the start_time, end_time window

# right now node types are PITSTOP, CIENT_WAREHOUSE and FUEL_PUMP
# we are going to remove the FUEL_PUMP nodes for computing sections
query = """
    select 
        vnt.id as vnt_id, journey_id, vnt.vehicle_number, vnt.node_type, vnt.node_id, 
        h.hub_code, vnt.stop_sequence, vnt.actual_in_timestamp, vnt.actual_out_timestamp
    from rivigo.vehicle_node_tracking vnt
    left join rivigo.Hub h on (vnt.node_type = 'PITSTOP' and h.id = vnt.node_id)
    where
        node_type in ('PITSTOP', 'CLIENT_WAREHOUSE')
        and journey_type = 'TRIP'
        and actual_out_timestamp >= {ts}
        and actual_in_timestamp <= {te}    
        and vnt.is_active = 1 
    order by journey_id asc, stop_sequence asc;
""".format( 
    ts = start_time.timestamp()*1000, # 1000 required since actual_out_time is in ms
    te = end_time.timestamp()*1000 
)
print(query)

fil_vnt_nodes_orig = get_mysql_df(query)

print()
print("number of filtered nodes = ", fil_vnt_nodes_orig.shape[0])
bad_nodes_filter = fil_vnt_nodes_orig.actual_in_timestamp > fil_vnt_nodes_orig.actual_out_timestamp # out should come after in
num_bad_nodes = fil_vnt_nodes_orig[ bad_nodes_filter ].shape[0]
print( "number of invalid nodes = {} ({:.2f}%)".format( num_bad_nodes, 100*num_bad_nodes / fil_vnt_nodes_orig.shape[0]  ))

fil_vnt_nodes = fil_vnt_nodes_orig[ ~bad_nodes_filter ]
print("number of valid vnt nodes = ", fil_vnt_nodes.shape[0])

def get_journey_id_sections_from_vnt_nodes(fil_vnt_nodes):
    d2 = fil_vnt_nodes.copy(deep=True).shift(-1)
    d3 = pd.merge(fil_vnt_nodes, d2, left_index=True, right_index=True)
    d4 = d3[ d3.journey_id_x == d3.journey_id_y ]
    print("intial number of sections =", d4.shape[0])

    d5 = d4[ d4.actual_out_timestamp_x <= d4.actual_in_timestamp_y ]
    print("physically possible sections = {} [ {:.2f}% ]".format( d5.shape[0], 100*d5.shape[0] / d4.shape[0] ))

    d6 = d5[ d5.node_id_x != d5.node_id_y ]
    print("after removing self sections =", d6.shape[0])

    d7 = d6[ (d6.node_type_x == 'PITSTOP') & (d6.node_type_y == 'PITSTOP') ]
    print("ps-to-ps sections =", d7.shape[0])

    fil_sections = d7[ 
        (d7.actual_out_timestamp_x >= start_time.timestamp()*1000) &
        (d7.actual_in_timestamp_y <= end_time.timestamp()*1000) 
    ]
    print("sections starting and ending inside the window =", fil_sections.shape[0])

    def check_and_transform_sections_df(df):
        df2 = df[ 
            (df.journey_id_x == df.journey_id_y)
            & (df.vehicle_number_x == df.vehicle_number_y)
            & (df.node_type_x == 'PITSTOP')
            & (df.node_type_y == 'PITSTOP')
            & (~df.hub_code_x.isnull())
            & (~df.hub_code_y.isnull())
            & (df.actual_in_timestamp_x <= df.actual_out_timestamp_x )
            & (df.actual_in_timestamp_y <= df.actual_out_timestamp_y )
            & (df.actual_out_timestamp_x <= df.actual_in_timestamp_y)
        ]
        assert df.shape[0] == df2.shape[0]

        df3 = pd.DataFrame()
        df3['journey_id'] = df['journey_id_x'].astype(int)
        df3['vehicle_number'] = df['vehicle_number_x']
        df3['src_ps'] = df['hub_code_x']
        df3['dest_ps'] = df['hub_code_y']
        df3['section'] = df['hub_code_x'] + '-' + df['hub_code_y']
        # df3["hub_id_pair"] = df.node_id_x.astype("str") + "_PITSTOP_" + (df.node_id_y.astype("int")).astype("str") + "_PITSTOP"
        df3['start_ts_unixms'] = df['actual_out_timestamp_x'].astype(int)
        df3['end_ts_unixms'] = df['actual_in_timestamp_y'].astype(int)
        return df3

    jid_sections_df = check_and_transform_sections_df(fil_sections)
    jid_sections_df.reset_index(drop=True, inplace=True)
    jid_sections_df['ujid'] = jid_sections_df.index
    return jid_sections_df
    
jid_sections = get_journey_id_sections_from_vnt_nodes(fil_vnt_nodes)
print("number of trips ==> " + str(jid_sections.shape[0]))
print("=====> done fetching trips.")

###############################################################################

#fil_trips_sdf = sqlContext.read.parquet("s3a://chronos-sensor-data/data/spark_data/pg-1/unsp/1/all_trips_24_oct_24_nov")
fil_trips_sdf = sqlContext.createDataFrame(jid_sections)
save("fil_trips_sdf", "all_trips_2018_nov_27_to_2019_feb_27")

vehicle_latlngs = ones_df.join(
    fil_trips_sdf,
    (F.col("sourceId") == F.col("vehicle_number")) &
    (F.col("ts_ms") >= F.col("start_ts_unixms")) &
    (F.col("ts_ms") <= F.col("end_ts_unixms"))
).select(
    "ujid",
    "journey_id",
    "section",
    "ts_ms",
    "lat",
    "lng"
)

save("vehicle_latlngs")


import cgeohash

@F.udf("string")
def geohash_encode_udf(lat, lng):
    try:
        return cgeohash.encode(lat, lng, 7)
    except:
        return None

latlng_with_geohash_base = vehicle_latlngs.withColumn("geohash", geohash_encode_udf("lat", "lng"))
save("latlng_with_geohash_base")

# latlng_with_geohash_base will contain those that have None geohashes
# filter out the none ones

nones = latlng_with_geohash_base.filter(F.col("geohash").isNull())
save("nones")

latlng_with_geohash = latlng_with_geohash_base.filter(F.col("geohash").isNotNull())
#save("latlng_with_geohash")
# will have to handle this that we wont have all non null values in geohash


# compute the time spent in each geohash in a naive way
import numpy as np

@pandas_udf(
    "ujid long, journey_id long, section string, geohash string, st long, et long, time_spent_sec double",
    PandasUDFType.GROUPED_MAP
)
def compute_time_spent_in_geohashes(df):
    df = df.sort_values('ts_ms')
    df['cons_geohash_id'] = (df.geohash != df.geohash.shift()).cumsum()
    df['n'] = df.groupby('cons_geohash_id')['geohash'].transform(np.size)
    os = df.shape[0]
    df = df[ df.n >= 2 ]
    ns = df.shape[0]
    
    columns = ['ujid', 'journey_id', 'section', 'geohash', 'st', 'et', 'time_spent_sec']
    if ns == 0:
        return pd.DataFrame(columns=['ujid', 'journey_id', 'section', 'geohash', 'st', 'et', 'time_spent_sec'])
    
    # after removing the one offs, some may combine again
    df['cons_geohash_id'] = (df.geohash != df.geohash.shift()).cumsum()
    
    
    def helper_gh_time(df):
        st = df.ts_ms.iloc[0]
        et = df.ts_ms.iloc[-1]
        time_spent_sec = (et - st)/1000
        return pd.DataFrame(columns=columns, data=[
            (df.ujid.iloc[0], df.journey_id.iloc[0], df.section.iloc[0], df.geohash.iloc[0], st, et, time_spent_sec)
        ])
        
    return df.groupby('cons_geohash_id').apply(helper_gh_time).reset_index(drop=True)

geohash_time_spent_sdf = latlng_with_geohash.groupby("ujid").apply(compute_time_spent_in_geohashes)
save("geohash_time_spent_sdf")



# Note need to filter all the geohashes that have less than say 50 trips before calling this

# using 15 trips as default value since when using 1 month data, min required support was
# 5 min, so when using 3 months data, proportionally increased required support 
def get_true_max_computer(min_support_size=15, support_time_window_size=5):
    @pandas_udf("section string, geohash string, num_trips long, true_max double, support_num_trips long", PandasUDFType.GROUPED_MAP)
    def compute_true_max_v1(df):
        # support_time_window_size = 5 # will check with ? minutes
        # min_support_size = 5 # atleast ? many trips should have supported this max value for it be now marked outlier  

        # contains data for a given section, geohash
        # verify that we have data for only section, geohash
        assert df.groupby(["section", "geohash"]).size().shape[0] == 1

        # TODO: will consider the times for all the 8 neighbors in v2
        df = df.sort_values("time_spent_sec", ascending=False)
        true_max = None
        support_size = None
        for _, row in df.iterrows():
            ts = row.time_spent_sec
            min_ts = ts - 60*support_time_window_size
            max_ts = ts + 60*support_time_window_size

            support_df = df.query("@min_ts <= time_spent_sec <= @max_ts")
            support_trips = support_df.ujid.nunique()
            if support_trips >= min_support_size:
                # got our true max
                true_max = ts
                support_size = support_trips
                break
        return pd.DataFrame(columns=['section', 'geohash', 'num_trips', 'true_max', 'support_num_trips'], data=[
            (df.section.iloc[0], df.geohash.iloc[0], df.ujid.nunique(), true_max, support_size)
        ])
    
    return compute_true_max_v1


min_num_trips = 50

fil_geohashes = geohash_time_spent_sdf.groupby("section", "geohash").agg(F.countDistinct("ujid").alias("num_trips")).filter(
    F.col("num_trips") >= min_num_trips
).join(
    geohash_time_spent_sdf,
    ["section", "geohash"]
)
save("fil_geohashes")


true_max_times_v1 = fil_geohashes.groupby("section", "geohash").apply(get_true_max_computer())
save("true_max_times_v1")
print("====> computed true max!")

################ load data from stoppage table and try and prediction ############
# --> this part failed while running as script (was getting out of memory error here)
# so will have to see
print("loading stoppages data...")
all_stoppages = get_mysql_df("""
select
  sa.id,
  sa.journey_id,
  h1.hub_code as prev_node_code,
  h2.hub_code as next_node_code,
  concat(h1.hub_code, '-', h2.hub_code) as section,
  sa.vehicle_number,
  sa.node_type,
  # TODO: figure out to what this node_id refers to ?
  sa.node_id,
  sa.location_string,
  sa.stoppage_start_timestamp,
  sa.stoppage_end_timestamp,
  sa.alert_type,

  st_y(stoppage_point) as lat, st_x(stoppage_point) as lng
from stoppage_alert sa
  # do joins to get prev vnt hub code
  left join vehicle_node_tracking vnt1
  on vnt1.id = sa.prev_vnt_id
  left join Hub h1
  on h1.id = vnt1.node_id
  # do joins to get next vnt hub code
  left join vehicle_node_tracking vnt2
  on vnt2.id = sa.next_vnt_id
  left join Hub h2
  on h2.id = vnt2.node_id

where
  stoppage_start_timestamp >= {ts}
  and stoppage_start_timestamp <= {te};
""".format(
    ts = start_time.timestamp()*1000, # 1000 required since actual_out_time is in ms
    te = end_time.timestamp()*1000
))
print(all_stoppages.shape[0])
f1_stoppages_sdf = sqlContext.createDataFrame(all_stoppages)
save("f1_stoppages_sdf", "all_stoppages_2018_nov_27_to_2019_feb_27")

f1_stoppages_sdf = f1_stoppages_sdf.withColumn("stoppage_time", (F.col("stoppage_end_timestamp") - F.col("stoppage_start_timestamp"))/1000)
f2_stoppages = f1_stoppages_sdf.join(true_max_times_v1, ["section", "geohash"]).filter(F.col('stoppage_time') >= F.col("true_max"))
    
save("f2_stoppages")

old_v_new_stoppages_comp = f2_stoppages.groupby("node_type").count().select("node_type", F.col("count").alias("new_count")).fillna({ "node_type": "?" }).join(
    f1_stoppages_sdf.groupby("node_type").count().select("node_type", F.col("count").alias("old_count")).fillna({ "node_type" : "?" }),
    "node_type",
    "outer"
).fillna({ "new_count": 0 }).withColumn("percent", 100*F.col("new_count") / F.col("old_count")).repartition(1)
save("old_v_new_stoppages_comp")
##############################################################################

print("====> done!")

