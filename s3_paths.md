# S3 Paths

This document contains the important s3 paths for some important already performed computations (so that don't have spend time recomputing them twice) / other resources.


## One second trimble location data frame

- **path**
	1. [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/one_sec_data_2018_sept_to_dec/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/one_sec_data_2018_sept_to_dec/?region=ap-southeast-1&tab=overview)
	2. [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/2/one_sec_data_2019_jan_to_27_feb/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/2/one_sec_data_2019_jan_to_27_feb/?region=ap-southeast-1&tab=overview)

- **description** - contains the one-second trimble location data stream in parquet df format
	1. contains data from 2018 Sep - Dec
	2. contains data from 2019 Jan - Feb (until 27th of feb)

- **generating scripts**
	1. [`s3://chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/1/dump_trimble_one_sec.py`](https://console.aws.amazon.com/s3/object/chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/1/dump_trimble_one_sec.py?region=ap-southeast-1&tab=overview)
	2. [`s3://chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py`](https://console.aws.amazon.com/s3/object/chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py?region=ap-southeast-1&tab=overview)

- **remarks**
	1. original source for the one-second stream (textfile) -> `s3://rivigo-sensor-data/trimble-fuel-continuous/year=*/month=*/day=*/*/*.gz`   (e.g -> [`s3://rivigo-sensor-data/trimble-fuel-continuous/year=2019/month=03/day=01/ip-10-0-1-124/trimble-fuel-continuous.log1551380462.gz`](https://s3.console.aws.amazon.com/s3/object/rivigo-sensor-data/trimble-fuel-continuous/year%253D2019/month%253D03/day%253D01/ip-10-0-1-124/trimble-fuel-continuous.log1551380462.gz?region=ap-southeast-1&tab=overview))
	2. schema of the data `<vehicle number>|<ts in ms>|<fuel>|<speed>|<lat>|<lng>|<ignore this>`. source from the trimble code where this is written -

```java
trimbleFuelContinuousSensorDataLogger.trace(
        "{}|{}|{}|{}|{}|{}|{}",
        data.getVehicleNumber(),
        data.getDate().getMillis(),
        CommonUtil.blankNull(data.getFuelValue()),
        CommonUtil.blankNull(data.getSpeed()),
        CommonUtil.blankNull(data.getLatitude()),
        CommonUtil.blankNull(data.getLongitude()),
        CommonUtil.blankNull(data.getGpsFix())
);
```


## Trip information joined with one second data

- **path** - [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/2/latlng_with_geohash_base/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/2/latlng_with_geohash_base/?region=ap-southeast-1&tab=overview)
- **description** - contains all ps-ps trips (for all sections) during the 3 month period (between 2018 Nov 27th - 2019 Feb 27th) joined with the one-second data in that period
- **generating script** - [`s3://chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py`](https://console.aws.amazon.com/s3/object/chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py?region=ap-southeast-1&tab=overview)
- **remarks** - [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/2/all_trips_2018_nov_27_to_2019_feb_27`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/2/all_trips_2018_nov_27_to_2019_feb_27/?region=ap-southeast-1&tab=overview) contains the trips in the above period that was used for joining

## Geohash time spent

- **path** - [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/2/geohash_time_spent_sdf/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/2/geohash_time_spent_sdf/?region=ap-southeast-1&tab=overview)
- **generating script** - [`s3://chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py`](https://console.aws.amazon.com/s3/object/chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/5/run_pipeline_for_all_sections_2018_nov_27_to_2019_feb_27.py?region=ap-southeast-1&tab=overview)
- **remarks**
	- computed for all ps to ps trips in the 3 month period (between 2018 Nov 27th - 2019 Feb 27th)
	- in this version, we have considered only those geohashes that more an one continous points.

## True max (for section, geohash)

- **path** - [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/2_2/true_max_times_v2/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/2_2/true_max_times_v2/?region=ap-southeast-1&tab=overview)
- **generating script** - [`s3://chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/6/compute_true_distinct_days.py`](https://console.aws.amazon.com/s3/object/chronos-sensor-data/data/spark_data/pg-1/automated_spark_job_run/6/compute_true_distinct_days.py?region=ap-southeast-1&tab=overview)
- **remarks**
	- computes the true max (maximum after removing outlier) time spent in section, geohash
	- this is also for all ps to ps trips in the 3 month period (between 2018 Nov 27th - 2019 Feb 27th)
	- to not be considered outlier each point must be supported by other points in atleast certain number of distinct days.
	- parameters used - `support_time_window_size=5` (5 minutes), `min_support_size=5` (must be seen on atleast 5 distinct days)
	- here only those section, geohash pairs have been considered that have alteast 10 distinct trips passing through in the entire window.

## Clustering geohashes into nodes

- **path** - [`s3://chronos-sensor-data/data/spark_data/pg-1/unsp/all_clusters_trained_on_3months_data/geohash_cluster_mapping/`](https://console.aws.amazon.com/s3/buckets/chronos-sensor-data/data/spark_data/pg-1/unsp/all_clusters_trained_on_3months_data/geohash_cluster_mapping/?region=ap-southeast-1&tab=overview)
- **description** - neighboring geohashes that have similar characteristics are clustered together to form nodes (for these nodes we shall provide the thresholds for raising tickets)
- **generating script** - [`cluster_formation_algorithm.py`](https://bitbucket.org/abkedia/prioritize_uns_tickets/src/master/cluster_formation_algorithm.py) - ran on notebook - called the `get_clusters` method with the true maxes df mentioned above.
- **remarks**
	- used a hardcoded criteria for ascertaining if two neighboring geohashes are part of the same cluster (given a certain section) --> `min(nt1, nt2) >= f1*max(nt1, nt2)` and `min(tm1, tm2) >= f2*max(tm1, tm2)` (where `nt` is the number of distinct trips passing through section, geohash and `tm` is the true max), with ->
		- `f1 = 0.6`
		- `f2 = 0.25`
