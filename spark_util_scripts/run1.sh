#!/bin/bash

# note in the ssh command ensure that doesn't ask for yes / no

./run_in_workers.py \
	"$1" \
	"ssh -o StrictHostKeyChecking=no -T -i ~/.ssh/prime-eta-spark.pem ec2-user@{ip}" \
	"sudo python3 -m pip install sklearn geohash2 pymysql py4j psycopg2 Cython pyarrow==0.9.0 pandas h2o==3.22.0.1 requests"

