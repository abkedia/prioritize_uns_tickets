#!/usr/bin/env python3

from subprocess import check_output
import multiprocessing.dummy as D
import sys
import json

# json list containing the ips as keys
# e.g - "[{'10.0.2.208': 'ig-2EYTSOVYAN5VA'}, {'10.0.2.57': 'ig-2EYTSOVYAN5VA'}, {'10.0.2.169': 'ig-1SVK22MXIDEBE'}, {'10.0.2.31': 'ig-2EYTSOVYAN5VA'}, {'10.0.2.222': 'ig-2EYTSOVYAN5VA'}]"
jin = sys.argv[1]
ips = [ list(e.keys())[0] for e in json.loads(jin.replace("'", '"'))]
print("Input ips are --\n", ips)

# command for sshing
# e.g. -> "ssh -T -i ~/.ssh/prime-eta-spark.pem ec2-user@{ip}"
# ip will be replaced with the ip of the machine
ssh_command = sys.argv[2]

# to run command
# e.g "sudo python3 -m pip install pandas"
command = sys.argv[3]

def run(ip):
    try:
        o = check_output("{ssh} '{runc}'".format(
                ssh=ssh_command.format(ip=ip),
                runc=command
            ), shell=True)
        return "{ip}\nSUCCESS\n{out}".format(ip=ip, out=o.decode())
    except:
        return "{ip}\nFAILURE".format(ip=ip)


p = D.Pool(len(ips))
outs = p.map(run, ips)
print ('\n\n'.join(outs))

