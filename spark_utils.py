"""
some generic spark utilies to make life a bit easier
"""

import pyspark.sql.functions as F

def rename(df, mapping):
	"""
	util to easily rename some columns of a spark df 
	mapping -> must a dict type that supports get
	"""
	return df.select(*[F.col(col).alias(mapping.get(col, col)) for col in df.columns])

def add_prefix(df, prefix):
	"""
	add a given prefix to all the columns of a spark df
	prefix -> can a single string / list of string / map of col -> string
	
	in case of a single string is provided the same prefix is added to all the columns
	"""

def groupby_count(df, cols, sort=True, compute_percent=True):
	adf = df.groupby(cols).count()
	if compute_percent:
		# TODO: learn window functions to compute the sum
		adf.withColumn('percent', 100*F.col('count') / )
