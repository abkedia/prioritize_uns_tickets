#!/bin/sh

# simple script to ssh into the cluster master
# don't want to fiddle with the ssh config file all the time to creating this
# here

MASTER_IP="10.0.2.8"
ssh -o StrictHostKeyChecking=no -i ~/.ssh/prime-eta-spark.pem ec2-user@$MASTER_IP
