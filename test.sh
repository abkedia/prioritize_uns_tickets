echo "hi"
job_script_s3_path=$1

if [ ! -z $job_script_s3_path ]; then
	echo "got job script s3 path - $job_script_s3_path"
    echo "will run the job and finish on termination"
    
    # trigger the main job and wait for it to be finished and then kill the cluster
    # will run this inside nohup so that jenkin job terminates immediately
    # to run multiple commands inside no hup ->
    # https://unix.stackexchange.com/questions/47230/how-to-execute-multiple-command-using-nohup
    
    # First load the command in a variable
read -r -d '' command << EOM
ssh -T -i emr_configs/prime-eta-spark.pem ec2-user@$master_ip '
    aws s3 cp $job_script_s3_path /tmp/job.sh
    chmod +x /tmp/job.sh
    sudo /tmp/job.sh > /dev/null
'
aws emr terminate-clusters --cluster-ids $cluster_id
EOM
    
    # run the command inside nohup in background
    # nohup sh -c "$command" &
	echo "$command"

fi
