"""
Fetches tickets from ticketing db and stoppage alerts
since we are using spark anyways here, this isn't the most optimal use of resources

right now we are squentially querying the db in chunks
should probably query the db parallely to get the max data throughput

need to probably work on this
"""


from datetime import timedelta
from datetime import datetime as dt
import pymysql
import pyspark.sql.functions as F
import pyspark.sql.types as T

import cgeohash

from get_trips_from_vnt import get_mysql_df, OrderedDictCursor

@F.udf("string")
def geohash_encode_udf(lat, lng):
  try:
    return cgeohash.encode(lat, lng, 7)
  except:
    return None

@F.udf("string")
def geohash_encode_udf2(locStr):
  # this takes in a location string of the form -> lat, lng
  # eg. "16.639965, 77.996205"
  try:
    lat, lng = [float(s.strip()) for s in locStr.split(",")]
    return cgeohash.encode(lat, lng, 7)
  except:
    return None


def batch_run(start, end, batch_days, batch_runner, *args, **kwargs):
  tstart = start
  while True:
    tend = min(tstart + timedelta(days=batch_days), end)
    yield batch_runner(tstart, tend, *args, **kwargs)
    if tend == end:
      break
    tstart = tend

def batch_fetch_from_sql(sqlContext, start, end,  batch_days, helper, *args, **kwargs):
  # helper should return pandas df for a single batch run
  combined_sdf = None
  it = batch_run(start, end, batch_days, helper, *args, **kwargs)
  for pdf in it:
    # pdf is a pandas data frame
    if pdf.shape[0] == 0:
      continue

    sdf = sqlContext.createDataFrame(pdf).repartition(1)
    if combined_sdf is None:
      combined_sdf = sdf
    else:
      combined_sdf = combined_sdf.union(sdf)
  
  return combined_sdf

def fetch_tv2_data_helper(start, end, silent=False):
  if not silent:
    print("fetching tickets created between {} to {} (GMT)".format(start, end))

  # TODO: pandas has a builting function to read data from sql
  # use that instead!
  # TODO: if start_ts and end_ts are untrusted inputs -> can lead to sql injection
  # escape them properly
  tv2_info = get_mysql_df("""
      select t.id,
        t.created_timestamp,
        t.closure_timestamp,
        tm.metadata_key,
        tm.metadata_value,
        tm.created_by
      from ticket t
      join ticket_metadata tm
      on t.id = tm.ticket_id
      where t.is_active and tm.is_active
        and ticket_type = 'UNSCHEDULED_STOPPAGE'
        and t.created_timestamp >= 1000*{}
        and t.created_timestamp < 1000*{};
  """.format(start.timestamp(), end.timestamp()), connect=lambda: pymysql.connect(
      host='ticketing-prod.c90oafloutlv.ap-southeast-1.rds.amazonaws.com',
      user='ak5883',
      password='e185b67',
      db='ticketing_v2',
      cursorclass=OrderedDictCursor
  ))

  if not silent:
    print("{} tickets fetched from ticketing db.".format( tv2_info['id'].nunique() ))

  return tv2_info

def fetch_tv2_data(sqlContext, start, end, silent=False):
  # this fetches data from ticketing v2 database 2 days at a time
  # and creates a spark data frame for that
  return batch_fetch_from_sql(sqlContext, start, end, 2, fetch_tv2_data_helper, silent=silent)

def fetch_alerts_helper(sa_start, sa_end, silent):
  if not silent:
    print("fetching stoppage alerts between {} to {} (GMT)".format(sa_start, sa_end))
  sa = get_mysql_df("""
    select id as alertId,
      st_y(stoppage_point) as lat,
      st_x(stoppage_point) as lng,
      stoppage_start_timestamp,
      stoppage_end_timestamp,
      ticket_generation_timestamp,
      node_type,
      alert_type,
      ((stoppage_end_timestamp - stoppage_start_timestamp) / 1000 / 60) as stoppage_min
    from stoppage_alert
    where stoppage_start_timestamp >= 1000*{}
      and stoppage_start_timestamp < 1000*{}
  """.format(sa_start.timestamp(), sa_end.timestamp()))
  if not silent:
    print("{} stoppages fetched from sa".format(sa.shape[0]))
  
  return sa

def fetch_alerts(sqlContext, start, end, silent):
    # will fetch alters in batches of 5 days
    return batch_fetch_from_sql(sqlContext, start, end, 5, fetch_alerts_helper, silent=silent)


def process_n_merge(tv2_sdf, sa_sdf):
  # start processing the data frames
  pc = """alertGenerationTimestamp
alertId
alertType
businessType
clientCode
clientId
closureComment
contactComment
conversationId
currentSection
failureReason
hubContactNumber
imageUrl
isTripVerified
journeyId
journeyType
location
latitude
longitude
locationString
nearestPitstop
pilotDetails
pilotId
pitstopCode
resolution
responsiblePilotEmpCode
responsiblePilotHomePS
stoppageDurationInMillis
stoppageLatitude
stoppageLongitude
title
tripCode
tripStatus
vehicleNumber
vehicleStopped
vehicleType
multimediaUrl
reason
comment""".splitlines()
  pc = [c.strip() for c in pc]

  # note this pivoting logic may not be 100% correct
  # in a very small number of cases, we might have same metadata key repeated more than once
  # but these are very rare cases
  # and handling by taking max in pivot
  tv2_sinfo = tv2_sdf.groupby('id').pivot('metadata_key', pc).agg( F.max("metadata_value") )
  
  # now also need to attach information on who created the reason and comment
  # making this slighty complicated to acommodate multiple entries for same key
  get_created_df = lambda f: tv2_sdf.filter(
      (F.col('metadata_key') == f) & (F.col('metadata_value').isNotNull())
    ).withColumn('s'+f, F.struct('metadata_value', 'created_by')).groupby('id').agg(
      F.min('s'+f).alias('m'+f)
    ).select('id', F.col('m'+f)['created_by'].alias(f + '_created_by'))

  for f in [ 'reason', 'comment' ]:
    tv2_sinfo = tv2_sinfo.join(
      get_created_df(f),
      'id',
      'left_outer'
    )

  # attach information from stoppage alerts info this
  tv2_sinfo_wsa = tv2_sinfo.join(sa_sdf, "alertId", "left_outer")

  ctses = tv2_sdf.groupby("id").agg(F.first("created_timestamp").alias("created_timestamp"), F.first("closure_timestamp").alias("closure_timestamp"))
  
  # authoritative df for the tickets
  tv2 = tv2_sinfo_wsa.join( ctses, "id", "left_outer" )

  # NOTES:
  # note that in the above df there are 3 lat, lngs
  # -> 2 from tickets table (latitude, longitude) consitituting location
  # --> aside actually sometimes latitude and longitude may not be individually
  # present but location is present
  # and stoppageLatitude, stoppageLongitude
  # and there another lat, lng from stoppage_alerts table

  # also there are atleast two stoppage times here
  # - from tickers table -> stoppageDurationInMillis
  # and also stoppage_min from stoppage_alerts

  # also there are two fields (may not necessarily be the same) alert_type (from sa) 
  # and alertType (from ticketing)

  # compute geohashes
  # we'll compute the corresponding geohashes for all the 3 different locations here
  # for now treating the location from stoppage table as canonical
  tv2_wgeohash = tv2.withColumn('geohash', geohash_encode_udf("lat", "lng"))\
                  .withColumn(
                    't_location_geohash1', geohash_encode_udf(
                      F.col("latitude").cast(T.DoubleType()), 
                      F.col("longitude").cast(T.DoubleType())
                  ))\
                  .withColumn(
                    't_location_geohash2', geohash_encode_udf2("location")
                  )\
                  .withColumn('t_stoppage_geohash', geohash_encode_udf(
                    F.col("stoppageLatitude").cast(T.DoubleType()), 
                    F.col("stoppageLongitude").cast(T.DoubleType())
                  ))

  return tv2_wgeohash


def get_uns_tickets(sqlContext, start_ts, end_ts, silent=False, sa_bufdays=10, saver=None):
  # start and end ts are unix timestamps in gmt in seconds
  # remember to pad out times to get better results

  # saver -> if it is provided then it shall be called to save the interim results
  # it should return a copy of the saved variable

  # note that this function uses spark because need to perform
  # pivoting and don't currently know that how to do it in pandas only
  # TODO: see if pivoting can be done in pandas
  start = dt.fromtimestamp(start_ts)
  end = dt.fromtimestamp(end_ts)
  sa_buftime = timedelta(days=sa_bufdays)
  sa_start = start - sa_buftime
  sa_end = end + sa_buftime

  if not silent:
    print("will fetch tickets from  - {} to {}".format(start, end))
    print("will fetch alerts from   - {} to {}".format(sa_start, sa_end))

  if not silent:
    print("fetching tickets...")
  
  tv2_sdf = fetch_tv2_data(sqlContext, start, end, silent=silent)
  
  if saver is not None:
    if not silent:
      print("saving tickets to s3...")
    tv2_sdf = saver(tv2_sdf, "raw_dumped_ticketing_v2")

  if not silent:
    print("fetching stoppage alerts...")
  
  sa_sdf = fetch_alerts(sqlContext, sa_start, sa_end, silent=silent)
  
  if saver is not None:
    if not silent:
      print("saving alerts to s3...")
    sa_sdf = saver(sa_sdf, "raw_dumped_stoppage_alerts")


  if not silent:
    print("merging and processing the data...")

  ans = process_n_merge(tv2_sdf, sa_sdf)

  if saver is not None:
    if not silent:
      print("saving processed data to s3...")
    ans = saver(ans, "processed_tickets_data")

  return ans
