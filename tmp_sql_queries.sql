# regarding cwh

SELECT
  cwh.id as node_id,
  'CLIENT_WAREHOUSE' as node_type,
  cwh.latitude,
  cwh.longitude,
  concat(
    IFNULL(cwh.code, 'null'), '|', 
    IFNULL(cwh.city_code, 'null'), '|', 
    IFNULL(cwh.city, 'null'), '|',
    IFNULL(cwh.name, 'null')
  ) as node_repr,
  concat(
    IFNULL(h.hub_code, 'null'), '|', 
    IFNULL(h.city, 'null'), '|', 
    IFNULL(h.state, 'null')
  ) as near_ps_repr
FROM Client_Ware_House as cwh
LEFT join Hub h
ON h.id = cwh.hub_id


# get sequence of nodes from vnt

select
  vnt.stop_sequence as '#',
  vnt.node_type,
  h1.hub_code,
#   cwh.code,
#   h2.hub_code,
  cwh.name,

  (actual_in_timestamp - 1551112200000)/1000/3600 as ahi,
  (actual_out_timestamp - 1551112200000)/1000/3600 as aho,
  (gps_in_timestamp - 1551112200000)/1000/3600 as ghi,
  (gps_out_timestamp - 1551112200000)/1000/3600 as gho,
  (scheduled_in_timestamp - 1551112200000)/1000/3600 as shi,
  (scheduled_out_timestamp - 1551112200000)/1000/3600 as sho,

  from_unixtime(vnt.actual_in_timestamp/1000 + 5.5*60*60) as ain_ist,
  from_unixtime(vnt.actual_out_timestamp/1000 + 5.5*60*60) as aout_ist,
  from_unixtime(vnt.gps_in_timestamp/1000 + 5.5*60*60) as gin_ist,
  from_unixtime(vnt.gps_out_timestamp/1000 + 5.5*60*60) as gout_ist,
  from_unixtime(vnt.scheduled_in_timestamp / 1000 + 5.5*60*60) as sin_ist,
  from_unixtime(vnt.scheduled_out_timestamp / 1000 + 5.5*60*60) as sout_ist,
  vnt.*
from vehicle_node_tracking vnt
left join Hub h1 on (vnt.node_type = 'PITSTOP' and vnt.node_id = h1.id)
left join Client_Ware_House cwh on (vnt.node_type = 'CLIENT_WAREHOUSE' and vnt.node_id = cwh.id)
# left join Hub h2 on cwh.hub_id = h2.id
where journey_id = 8015494
order by stop_sequence, scheduled_in_timestamp;


## or simpler

select
  journey_id,
  node_type,
  h1.hub_code,
  from_unixtime(vnt.actual_in_timestamp/1000 + 5.5*60*60) as ain_ist,
  from_unixtime(vnt.actual_out_timestamp/1000 + 5.5*60*60) as aout_ist
from vehicle_node_tracking vnt
left join Hub h1 on (vnt.node_type = 'PITSTOP' and vnt.node_id = h1.id)
where journey_id = 8015494
  and node_type in ('PITSTOP', 'CLIENT_WAREHOUSE')
order by stop_sequence, scheduled_in_timestamp;


