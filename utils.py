# function to compute the haversine distance between a pair of points

HD = DistanceMetric.get_metric('haversine')
def hd(pl):
    """
    function to compute the haversine distance between a list of
    lat, lng points
    """
    # pl -> list of points as [ [lat1, lng1], [ lat2, lng2 ], ... ]
    # find the max distance (in m) amongs those
    return HD.pairwise(np.radians( pl )).max()*6371*1000


# 20.518595, 78.768799
# 20.510036, 78.903075
# distance -> 14.01-03 km
# answer from above -> 14016.3 m



